#
# Makefile 
# backtrack
#

CC=gcc
CFLAGS=-g -std=c99 -Werror -Wall -Wextra -Wformat -Wformat-security -pedantic
LIBS=-lm

FILES=nqueens.c main.c
FILES_PERM=util.c perm.c main_perm.c
FILES_COMB=util.c comb.c main_comb.c
CLEAN=main main_perm main_comb

all: main main_perm main_comb

main: ${FILES}
	${CC} ${CFLAGS} $^ -o main ${LIBS}

main_perm: ${FILES_PERM}
	${CC} ${CFLAGS} $^ -o main_perm ${LIBS}

main_comb: ${FILES_COMB}
	${CC} ${CFLAGS} $^ -o main_comb ${LIBS}

clean: ${CLEAN}
	rm $^
