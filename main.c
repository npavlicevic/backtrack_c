#include "nqueens.h"

int main() {
  int *cols = calloc(8, sizeof(int));
  nqueens(cols, 0, 8);
  free(cols);
  return 0;
}
