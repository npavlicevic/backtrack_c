#ifndef UTIL_H
#define UTIL_H
#include <stdlib.h>
#include <stdio.h>

void array_init(int *arr, int n);
void array_swap(int *arr, int from, int to);
void array_print(int *arr, int n);
#endif
