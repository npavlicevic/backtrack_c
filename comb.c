#include "comb.h"
void comb(int *choices, int choice, int k, int *from, int from_, int n) {
  int i;
  if(choice >= k) {
    array_print(choices, choice);
  } else {
    for(i = from_; i < n; i++) {
      choices[choice] = from[i];
      comb(choices, choice+1, k, from, i+1, n);
    }
  }
}
