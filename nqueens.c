#include "nqueens.h"
int nqueens_safe(int *cols, int col) {
  int i;
  for(i = 0; i < col; i++) {
    if(cols[col] == cols[i]) return 0;
    if(abs(cols[col] - cols[i]) == col - i) return 0;
  }
  return 1;
}
void nqueens(int *cols, int col, int colsn) {
  int i;
  if(col >= colsn) {
    nqueens_print(cols, colsn);
  } else {
    for(i = 0; i < colsn; i++) {
      cols[col] = i;
      if(nqueens_safe(cols, col)) {
        nqueens(cols, col + 1, colsn);
      }
    }
  }
}
void nqueens_print(int *cols, int colsn) {
  int i,j;
  for(i = 0; i < colsn; i++) {
    for(j = 0; j < colsn; j++) {
      if(cols[j] == i) printf("Q ");
      else printf("+ ");
    }
    printf("\n");
  }
  printf("\n");
}
