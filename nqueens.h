#ifndef NQUEENS_H
#define NQUEENS_H
#include <stdlib.h>
#include <stdio.h>
int nqueens_safe(int *cols, int col);
void nqueens(int *cols, int col, int colsn);
void nqueens_print(int *cols, int colsn);
#endif
