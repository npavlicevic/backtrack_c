#include "util.h"

void array_init(int *arr, int n) {
  int i;
  for(i = 0; i < n; i++) {
    arr[i] = rand() % 128;
  }
}
void array_swap(int *arr, int from, int to) {
  int temp = arr[to];
  arr[to] = arr[from];
  arr[from] = temp;
}
void array_print(int *arr, int n) {
  int i;
  for(i = 0; i < n; i++) {
    printf("%d ", arr[i]);
  }
  printf("\n");
}
