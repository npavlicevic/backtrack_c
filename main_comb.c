#include "comb.h"

int main() {
  int n,k;
  fscanf(stdin, "%d", &n);
  int *from = calloc(n, sizeof(int));
  array_init(from, n);
  array_print(from, n);
  fscanf(stdin, "%d", &k);
  int *choices = calloc(k, sizeof(int));
  comb(choices, 0, k, from, 0, n);
  free(from);
  free(choices);
  return 0;
}
