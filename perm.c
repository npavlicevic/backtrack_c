#include "perm.h"
void perm(int *choices, int choice, int n) {
  int i;
  if(choice >= n) {
    array_print(choices, n);
  } else {
    for(i = choice; i < n; i++) {
      array_swap(choices, choice, i);
      perm(choices, choice+1, n);
      array_swap(choices, choice, i);
    }
  }
}
