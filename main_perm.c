#include "perm.h"

int main() {
  int n;
  fscanf(stdin, "%d", &n);
  int *choices = calloc(n, sizeof(int));
  array_init(choices, n);
  array_print(choices, n);
  perm(choices, 0, n);
  free(choices);
  return 0;
}
